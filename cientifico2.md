# Científico 2.0

## Parte 1

* Uso de herramientas generales.
* Mejoran nuestra productividad.
* Son accesibles.
* Primera parte
    * Producción de documentos.
    * Control de versiones.
* Esta actividad es un taller.
* Vamos haciendo a la vez que hablamos.

## Producción de documentos

* Usted necesita un editor de texto.
* En este curso usamos VScode.
* https://code.visualstudio.com/

## Notas rápidas
* Markdown es un lenguaje "markup". 
* Para los no expertos: combinar texto plano con marcas que definen un formato.
* El formato es sencillo de entender. Un documento en MD es legible.
* Es fácil de usar.
* Guía MD: https://www.markdownguide.org/cheat-sheet/

## Aprendo MD: Títulos y listas

* Título, subtítulo, subsub: "# Bla, ## Bla, ### Bla"
* Listas (todo el mundo necesita listas): "* item"
* Listas numéradas: "1. item 1. item"
* Ejemplo:
1. kaka
1. keke
    1. hh
1. jj
* Sublistas: usar sangría

## Aprendo MD: Tablas

* Tablas: 

"|Título col 1|Título col 2|Título col 3|
|:---|:---:|---:|
|fila 1 col 1|fila 1 col 2|fila 1 col 3|"

|Título col 1|Título col 2|Título col 3|
|:---|:---:|---:|
|fila 1 col 1|fila 1 col 2|fila 1 col 3|

## Aprendo MD: otras cosas

* No hace falta mucho más
* MD con sabores:
    * MD y html son amigos
    * MD y latex son amigos

## Transformando MD

* Tengo un MD, se ve chévere en la web
* ¿Cómo lo convierto en pdf?
* pandoc

## PANDOC

* Pandoc es un conversor universal de documentos
* https://pandoc.org/
* En la terminal linux: pandoc -i cosa.md -o cosa.pdf
* No hace milagros, pero casi.

## LaTeX
* Es un sistema de software para producir documentos.
* Funciona como un lenguaje de programación.
* Preámbulo + código.
* Al compilar se produce el documento.
* https://www.latex-project.org/get/
* LaTeX en la web: https://www.overleaf.com/

## ¿Por qué LaTeX?
* Mucho control sobre los parámetros del documento.
* Fácil de controlar (código plano).
* Interoperable.

## ¿Cómo se ve LaTeX?
Veamos un documento.

## Ciclo de vida del documento
* Todo el mundo ha visto esto
    * TesisJALVersionFinal.pdf
    * TesisJALVersionFinalRevisada.pdf
    * TesisJALLVersionFinalNueva.pdf
    * TesisJALVersionFinalFinal.pdf
    * Etc
* git fixes this!

## git
* Es un sistema de control de versiones distribuido.
* Mantiene el historial de cambios declarados en un proyecto.
* https://git-scm.com/
* Funciona super bien para monitorear los cambios de código.
    * Si usas c++ o python, muy bien.
    * Idea: ¿si uso .md o .tex?

## ¿Cómo funciona git?
* Comandos fundamentales
    * add: añade una lista de archivos a la lista de cambios
    * commit: completa un cambio en el proyecto sobre los archivos añadidos. añade una marca única y asociada al momento en que se hace.
    * push: (hacia dónde)
    * pull: (desde dónde)
    * Muchos otros comandos.

## Servicios remotos git

* github: muy popular https://github.com/
* gitlab: también muy usado https://gitlab.com/

## Otra vez el ciclo de vida del documento
* add TesisJAL.tex
* commit -m "Version Final"
* add TesisJAL.tex
* commit -m "Version Final Revisada"
* add TesisJAL.tex
* commit -m "Version Final Nueva"
* add TesisJAL.tex
* commit -m "Version Final Final"
* add TesisJAL.tex
* commit -m "aagghhhhh!"

## Administración de un proyecto
* Caso de uso: tareas
    * Se genera un repositorio (proyecto) donde un estudiante entrega avances de sus tareas.
    * El docente observa los avances en el desarrollo del proyecto, las fechas de culminación, etc.
* Caso de uso: trabajos de grado
    * Dirección del proyecto observa las entregas del documento.
    * Se controlan rondas de revisión y cambios en un único documento.
    * Se definen "releases" con hitos importantes. Por ejemplo, entrega a los jurados.
    * Los "releases" permiten identificar fechas que quedan registradas.
* Trabajos de ascenso: idem trabajos de grado.

## Un proyecto compartido gitlab
* Usuarios de un proyecto
* Haciendo fork

## Vamos a clase
* Moodle (wikipedia): es un sistema de gestión de aprendizaje (LMS), gratuito y de código abierto.
* Es un LMS muy extendido
* Es potente
* Nuestra relación con moodle es complicada
* Estará por aquí algún tiempo
* https://moodle.org/

## Moodle: roles
* Profesor editor
* Profesor no editor
* Estudiante
* Invitado
* Otros roles de administración

## Moodle: recursos y actividades (todo el mundo sabe que hay un curso)
* Recursos: permiten la organización de los contenidos del curso
* Ejemplos de recursos: son la descarga de archivos, las zonas de texto, los enlaces, etc.
* Actividades: son piezas del sistema que permiten la interacción efectiva de los estudiantes.
* Ejemplos de actividades: Tareas, cuestionarios, encuestas, foros, etc.

## Vamos a publicarnos
* Registro: OBS Studio
* https://obsproject.com/
* Edito: Shotcut
* https://shotcut.org/
* Publico: Youtube
* https://studio.youtube.com/

## Vamos a afinar el perfil en linea
* Nuestra presencia en linea es importante.
* Nuestra presencia en linea es una responsbilidad institucional.
* google scholar es muy utilizado.
* Una vez que se ha configurado el perfil es simple de mantener.
* https://scholar.google.com/

## Vamos a afinar el perfil en linea (2)
* ORCID: Open Researcher and Contributor ID: https://orcid.org/0000-0003-3613-3406
* Identificación única para la persona: evita confusiones con el nombre.
* Se puede conectar con la institución, permite algunos tipos de búsquedas.
* https://orcid.org/
* Podemos usar el formato BibTeX (LaTeX) para exportar nuestro record de scholar google e importarlo en orcid.

## Vamos a colaborar
* DOI: Digital Object Identifier
* Es un nombre único para un objeto digital
* Artículos de investigación
* Piezas de documentación
* Datos

## ¿Quién tiene los DOI?
* Las editoriales de toda la vida
* Otro tipo de organización que se comprometa a dar el respaldo en el tiempo
* ZENODO: Es un repositorio de uso general (OpenAIRE CERN)
* https://zenodo.org/

## Para qué usar zenodo
* Identificar literatura gris
* Sets de datos
* Notas de cursos
* Posters